package ru.rt;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static Scanner s = new Scanner(System.in);
    private static RLE rle = new RLE();
    private static RLEOptimized rleOptimized = new RLEOptimized();

    public static void main(String[] args) throws Exception {
       while (true){
           System.out.println("Выберите версию алгоритма:");
           System.out.println("1. Простой RLE");
           System.out.println("2. Оптимизированный RLE");
           int i = s.nextInt();
           if(i == 1){
               func(rle);
           }else if(i == 2){
               func(rleOptimized);
           }
       }
    }

    private static void func(RLE rle) throws IOException {
        int i = chooseOperation();
        if(i == 1){
            File f = getFile();
            byte[] bytes = Files.readAllBytes(f.toPath());
            byte[] encode = rle.encode(bytes);
            File fEncoded = new File(f.getParent() + "\\" + getNewName());
            Files.write(fEncoded.toPath(),encode);
        }else if(i == 2){
            File f = getFile();
            byte[] bytes = Files.readAllBytes(f.toPath());
            byte[] decode = rle.decode(bytes);
            File fDecoded = new File(f.getParent() + "\\" + getNewName());
            Files.write(fDecoded.toPath(),decode);
        }
        System.out.println("Готово!");
    }


    private static int chooseOperation(){
        System.out.println("Выберите операцию:");
        System.out.println("1. Сжатие");
        System.out.println("2. Распаковка");
        return s.nextInt();
    }

    private static String getNewName(){
        System.out.println("Введите новое имя файла:");
        return s.next();
    }

    private static File getFile() {
        File f;
        while (true){
            System.out.println("Укажите полный путь к файлу:");
            String path = s.next();
            f =  new File(path);
            if(!f.exists()){
                System.out.println("Такого файла не существуует.");
            }else{
                break;
            }
        }
        return f;
    }

}

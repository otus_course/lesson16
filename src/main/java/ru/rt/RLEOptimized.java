package ru.rt;

import java.util.Arrays;

public class RLEOptimized extends RLE{
    @Override
    public byte[] encode(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return new byte[0];
        }

        int index = 0;
        byte[] zipped = new byte[bytes.length*2];

        int count = 1;
        int singleSize = 0;
        byte previous = bytes[0];
        byte current;

        for (int i = 1; i < bytes.length; i++) {
            current = bytes[i];
            if (current == previous) {
                count++;
            } else {
                if(count == 1){
                    singleSize++;
                }else{
                    if(singleSize>0){
                        index = append(zipped,index,singleSize,Arrays.copyOfRange(bytes, i - singleSize - count, i - count));
                        singleSize = 0;
                    }
                    index = append(zipped,index,count,previous);
                    count = 1;
                }
            }
            previous = current;

        }
        if(singleSize>0){
            index = put(zipped,singleSize,index,Arrays.copyOfRange(bytes, bytes.length - singleSize - count, bytes.length - count));
        }

        index = put(zipped,count,index,previous);
        return Arrays.copyOfRange(zipped,0,index);
    }

    private static int MAX = 127;
    private static int append(byte[] zipped, int index, int singleSize, byte[] previous) {
        while (singleSize > MAX){
            index = put(zipped,MAX,index,previous);
            singleSize -= MAX;
        }

        return put(zipped,singleSize,index,previous);
    }

    private static int append(byte[] zipped, int index, int count, byte previous) {
        while (count > MAX){
            index = put(zipped,MAX,index,previous);
            count -= MAX ;
        }
        return put(zipped,count,index,previous);
    }

    private static int put(byte[] zipped, int count, int index,byte[] previous) {
        zipped[index++] = (byte)-count;
        for (int i = 0; i < previous.length; i++) {
            zipped[index++] = previous[i];
        }
        return index;
    }

    private static int put(byte[] zipped, int count, int index,byte previous) {
        zipped[index++] = (byte)count;
        zipped[index++] = (byte)previous;
        return index;
    }

    @Override
    public byte[] decode( byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return new byte[0];
        }

        int index = 0;
        byte[] unzipped = new byte[bytes.length*2];

        for (int i = 0; i < bytes.length;){
            int cnt = (int)bytes[i];
            if(cnt > 0){
                byte c = bytes[i+1];
                for (; cnt > 0 ; cnt--) {
                    if(index == unzipped.length - 1){
                        unzipped = nArray(unzipped);
                    }
                    unzipped[index++]=c;
                }
                i+=2;
            }else{
                while (cnt<0){
                    byte c = bytes[++i];
                    if(index == unzipped.length - 1){
                        unzipped = nArray(unzipped);
                    }
                    unzipped[index++]=c;
                    cnt++;
                }
                i++;
            }

        }

        return Arrays.copyOfRange(unzipped,0,index);
    }

    private static byte[] nArray(byte[] unzipped) {
        byte[] nArr = new byte[unzipped.length*2];
        for (int i = 0; i < unzipped.length; i++) {
            nArr[i] = unzipped[i];
        }
        return nArr;
    }
}

package ru.rt;

import java.util.Arrays;

public class RLE {
    public  byte[] encode(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return new byte[0];
        }

        int index = 0;
        byte[] zipped = new byte[bytes.length*2];

        int count = 1;
        byte previous = bytes[0];
        byte current;

        for (int i = 1; i < bytes.length; i++) {
            current = bytes[i];
            if (current == previous) {
                count++;
            } else {
                index = append(zipped,index,count,previous);
                count = 1;
            }
            previous = current;

        }
        zipped[index++] = (byte)count;
        zipped[index++] =(byte) previous;
        return Arrays.copyOfRange(zipped,0,index);
    }

    private static int MAX = 127;
    private static int append(byte[] zipped, int index, int count, byte previous) {
        while (count > MAX){
            zipped[index++] = (byte)MAX;
            zipped[index++] = (byte)previous;
            count -= MAX ;
        }
        zipped[index++] = (byte)count;
        zipped[index++] =(byte) previous;
        return index;
    }


    public byte[] decode( byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return new byte[0];
        }

        int index = 0;
        byte[] unzipped = new byte[bytes.length*2];

        for (int i = 0; i < bytes.length;i += 2){
            int cnt = (int)bytes[i];
            byte c = bytes[i+1];
            for (; cnt > 0 ; cnt--) {
                if(index == unzipped.length - 1){
                    unzipped = nArray(unzipped);
                }
                unzipped[index++]=c;
            }
        }

        return Arrays.copyOfRange(unzipped,0,index);
    }

    private static byte[] nArray(byte[] unzipped) {
        byte[] nArr = new byte[unzipped.length*2];
        for (int i = 0; i < unzipped.length; i++) {
            nArr[i] = unzipped[i];
        }
        return nArr;
    }
}
